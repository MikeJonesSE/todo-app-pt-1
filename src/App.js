import React, { useState } from "react";
import { todos as todosList } from "./todos";
import { v4 as uuid } from "uuid"
import {BrowserRouter as  Route, Switch, NavLink} from "react-router-dom"
import TodoList from "./components/todolist/TodoList";



function App () {
 
const [todos, setTodos] = useState(todosList)
const [input, setInput] = useState("")


const AddTodo = (event) => {
  if(event.which === 13) {
    const newId = uuid()

    const newTodo ={
      "userId": 1,
      "id": newId,
      "title": input,
      "completed": false
    }
    
    const newTodos = {...todos}
    newTodos[newId] = newTodo
    setTodos(newTodos)
    setInput("")
  }
}


const toggleComplete = (id) => {
  const newTodos = {...todos}
  newTodos[id].completed = !newTodos[id].completed
  setTodos(newTodos)
}


  const deleteTodo = (id) => {
    const newTodos = {...todos}
    delete newTodos[id]
    setTodos(newTodos)
  }
  

  const clearAllCompleted = (id) => {
    const newTodos = {...todos}
    for(const todo in newTodos){
      if(newTodos[todo].completed){
        delete newTodos[todo]
      }
    }
    setTodos(newTodos)
  }



    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
          value={input}
           onChange={(event) => setInput(event.target.value)}
           onKeyDown={(event) => AddTodo(event)}
           className="new-todo" 
           placeholder="What needs to be done?" autoFocus />
        </header>
      <Switch>
        <Route exact path="/">

        <TodoList todos={Object.values(todos)} 
        toggleComplete={toggleComplete}
        deleteTodo={deleteTodo}
        />

        </Route>

        <Route path="/active">
          <TodoList
          todos={Object.values(todos).filter(todo =>
            !todo.completed
            )}
            toggleComplete={toggleComplete}
            deleteTodo={deleteTodo}
            />
        </Route>

        <Route path="/completed">
          <TodoList
          todos={Object.values(todos).filter(todo =>
            todo.completed
            )}
            toggleComplete={toggleComplete}
            deleteTodo={deleteTodo}
            />
        </Route>
      </Switch>






       
        <footer className="footer">
          <span className="todo-count">
            <strong>
              {Object.values(todos).filter((todo : {...}) => !todo.completed).length}
              </strong> {" "} item(s) left
          </span>
          <ul className="filters">
    <li>
      <NavLink exact to="/" activeClassName='selected'>All</NavLink>
    </li>
    <li>
      <NavLink to="/active" activeClassName='selected'>Active</NavLink>
    </li>
    <li>
      <NavLink to="/completed" activeClassName='selected'>Completed</NavLink>
    </li>
  </ul>
          <button className="clear-completed"
          onClick={() => clearAllCompleted()}
          >Clear completed</button>
        </footer>
      </section>
    );
  
}



export default App;
